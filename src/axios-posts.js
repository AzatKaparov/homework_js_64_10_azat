import axios from "axios";

const axiosPosts = axios.create({
    baseURL: 'https://lab-64-5a988-default-rtdb.firebaseio.com/'
})

export default axiosPosts;