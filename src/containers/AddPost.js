import React, {useEffect, useState} from 'react';
import MyNavbar from '../components/UI/MyNavbar';
import {Form, Button} from 'react-bootstrap';
import {getDate} from "../constants";
import axiosPosts from "../axios-posts";

const AddPost = ({history, match}) => {
    const [author, setAuthor] = useState({
        name: '',
        postText: ''
    });

    useEffect(() => {
        if (match.params.id) {
            const fetchData = async () => {
                const postResponse = await axiosPosts.get(`./posts/${match.params.id}.json`);
                const data = postResponse.data;
                setAuthor({
                    name: data.author,
                    postText: data.text
                });
            };
            fetchData().catch(console.error);
        }
    }, []);

    const authorDataChanged = e => {
        const name = e.target.name;
        const value = e.target.value;

        setAuthor(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const postSendHandler = async e => {
        e.preventDefault();
        const post = {
            author: author.name,
            text: author.postText,
            date: getDate(),
        };

        if (!match.params.id) {
            try {
                await axiosPosts.post('./posts.json', post);
            } finally {
                history.push('/');
            }
        } else {
            try {
                await axiosPosts.put(`./posts/${match.params.id}.json`, post);
            } finally {
                history.replace(`./posts/${match.params.id}`);
            }
        }
    };


    return (
        <div className="container">
            <MyNavbar/>
            <Form onSubmit={postSendHandler}>
                <Form.Group className="mb-3" controlId="authorName">
                    <Form.Label>Your name</Form.Label>
                    <Form.Control
                        type="text"
                        name="name"
                        value={author.name}
                        onChange={authorDataChanged}
                        placeholder="John Doe..."
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="authorPostText">
                    <Form.Label>Post text</Form.Label>
                    <Form.Control
                        as="textarea"
                        name="postText"
                        rows={3}
                        value={author.postText}
                        onChange={authorDataChanged}
                    />
                </Form.Group>
                <Button type="submit">Confirm</Button>
            </Form>
        </div>
    );
};

export default AddPost;