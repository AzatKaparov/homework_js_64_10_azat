import React, {useEffect, useState} from 'react';
import axiosPosts from "../axios-posts";
import MyNavbar from "../components/UI/MyNavbar";
import PostPreview from "../components/Home/PostPreview";
import './Home.css';


const Home = () => {
    const [posts, setPosts] = useState([]);

    const handleDelete = id => {
        axiosPosts.delete(`./posts/${id}.json`)
            .then(() => {
                setPosts(posts.filter(item => (item.id !== id)));
            });
    };

    useEffect(() => {
        const fetchData = async () => {
            const postResponse = await axiosPosts.get('./posts.json');
            const newArray = [];
            for (let key in postResponse.data) {
                let item = postResponse.data[key];
                const newPost = {id: key, ...item}
                newArray.push(newPost)
            }
            setPosts(newArray);
        }
        fetchData().catch(console.error);
    }, []);

    return (
        <div className="container">
            <MyNavbar/>
            <div className="posts-block">
                {posts.map(post => (
                    <PostPreview
                        id={post.id}
                        author={post.author}
                        text={post.text}
                        date={post.date}
                        deleted={e => {
                            e.preventDefault();
                            handleDelete(post.id);
                        }}
                        key={post.id}
                    />
                ))}
            </div>
        </div>
    );
};

export default Home;