import React, {useEffect, useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import MyNavbar from "./UI/MyNavbar";
import axiosPosts from "../axios-posts";


const FullPost = ({ match, history }) => {
    const [post, setPost] = useState(null)

    const handleDelete = () => {
        axiosPosts.delete(`./posts/${match.params.id}.json`)
            .then(() => {
                setPost(null);
                history.replace('/');
            });
    };

    useEffect(() => {
        const fetchData = async () => {
            const postResponse = await axiosPosts.get(`./posts/${match.params.id}.json`);
            setPost(postResponse.data);
        };
        fetchData().catch(console.error);
    }, []);

    if (post !== null) {
        return (
            <div className="container">
                <MyNavbar/>
                <Card className="mb-2">
                    <Card.Header>{post.date}</Card.Header>
                    <Card.Body>
                        <Card.Title>{post.author}</Card.Title>
                        <Card.Text>
                            {post.text}
                        </Card.Text>
                        <Button onClick={handleDelete} variant="danger">Delete</Button>
                        <Button variant="success" className="ml-2" href={`/posts/${match.params.id}/edit`}>Edit</Button>
                    </Card.Body>
                </Card>
            </div>
        );
    } else {
        return null;
    }
};

export default FullPost;
