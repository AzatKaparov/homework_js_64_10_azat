import React from 'react';
import {Card, Button} from 'react-bootstrap';


const PostPreview = ({ text, date, author, id}) => {
    return (
        <Card className="mb-2">
            <Card.Header>{date}</Card.Header>
            <Card.Body>
                <Card.Title>{author}</Card.Title>
                <Card.Text>
                    {text.substr(0, 20) + "..."}
                </Card.Text>
                <Button variant="primary" href={`/posts/${id}`}>Read more</Button>
            </Card.Body>
        </Card>
    );
};

export default PostPreview;