import React from 'react';
import MyNavbar from "../UI/MyNavbar";

const Contacts = () => {
    return (
        <div className="container">
            <MyNavbar/>
            <h1>You can find me in:</h1>
            <ul>
                <li>My phone: +996 502 004 536</li>
                <li>My email: azatkaparov843@gmail.com</li>
                <li>My IG: @azat-kaparov</li>
            </ul>
        </div>
    );
};

export default Contacts;