import {Route, Switch, BrowserRouter} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Home from "./containers/Home";
import AddPost from "./containers/AddPost";
import FullPost from "./components/FullPost";
import About from "./components/About/About";
import Contacts from "./components/Contacts/Contacts";

function App() {
  return (
    <div className="app">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/posts/add" exact component={AddPost}/>
          <Route path="/posts/:id/edit" exact component={AddPost}/>
          <Route
              path='/posts/:id'
              render={(props) => <FullPost {...props} />}
          />
          <Route path="/about" component={About} />
          <Route path="/contacts" component={Contacts} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
